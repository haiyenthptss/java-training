package lession01;

public class Main {
    public static void main(String[] args) {
        System.out.println("Hello world!");     // Câu lệnh in thông tin ra màn hình


        // In thông tin cá nhân ra ngoài màn hình console
        System.out.println("*************************************************");
        System.out.println("***** DuyBG");
        System.out.println("***** I'm an automation engineer");
        System.out.println("*************************************************");


        // Khai bao bien
        int index = 0;                          // Khai bao bien index voi kieu du lieu la kieu so (int)
        long timestamp = 1686106190;            // Khai bao bien index voi kieu du lieu la kieu so (int)

        boolean isChecked = false;              // Khai bao bien boolean co kieu du lieu tra ve la true/false
        String name = "DuyBG";                  // Khai bao bien String: ten bien name va gan gia tri bang DuyBG
    }
}